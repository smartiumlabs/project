// import { Template } from 'meteor/templating';
// import { ReactiveVar } from 'meteor/reactive-var';

Meteor.setInterval(function () {
  Session.set('time', new Date());
}, 1000);

Template.registerHelper('formatTime', (time)=> {
  return `<small>${moment(time).format('DD [de] MMMM [de] YYYY')}</small><br/ ><big>${moment(time).format('hh[hrs ]mm[min ]ss[seg]')}</big>`;
});

Template.body.helpers({
  hours: _.range(0, 12),

  degrees: function () {
    return 30 * this;
  },

  handData: function () {
    var time = Session.get('time') || new Date();
    return { hourDegrees: time.getHours() * 30,
             minuteDegrees: time.getMinutes() * 6,
             secondDegrees: time.getSeconds() * 6 };
  },

  radial: function (angleDegrees,
                    startFraction,
                    endFraction) {
    var r = 100;
    var radians = (angleDegrees-90) / 180 * Math.PI;

    return {
      x1: r * startFraction * Math.cos(radians),
      y1: r * startFraction * Math.sin(radians),
      x2: r * endFraction * Math.cos(radians),
      y2: r * endFraction * Math.sin(radians)
    };
  }
});

Template.interact.onCreated(()=> {
  animation = new ReactiveVar();
  hover = new ReactiveVar();
});

Template.interact.helpers({
  times() {
    return Times.find({}, {sort: {epoch: -1}});
  },

  animation(id) {
    if (animation.get() === id) {
      return 'zoomOutDown';
    }
    else {
      return 'zoomIn';
    }
  },

  hidden(id) {
    return hover.get() === id ? false : true;
  }
});

Template.interact.events({
  'click button'(e) {
    Times.insert({
      time: new Date(),
      epoch: new Date().valueOf()
    });
  },

  'mouseenter li'(e) {
    hover.set(this._id);
  },

  'mouseleave li'(e) {
    hover.set();
  },

  'click li span'(e) {
    animation.set(this._id);
    Meteor.setTimeout(()=> {
      Times.remove(this._id);
    }, 600);
  }
});
